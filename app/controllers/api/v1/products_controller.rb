module Api
    module V1
        class ProductsController < ApplicationController
            #http_basic_authenticate_with :name =>"arp", :password =>"arp"
            protect_from_forgery with: :null_session
            before_filter :restrict_access
            
            respond_to :json
            def index
                respond_with Product.all
            end
            def show
                respond_with Product.find(params[:id])
            end
            
            def create
                respond_with Product.create(params_product)
            end
            def update
                respond_with Product.update(params[:id],params_product)
            end
            def destroy
                respond_with Product.destroy(params[:id])
            end
            
            private 
                def params_product
                    params.require(:product).permit(:name,:description,:price)
                end
                
                def restrict_access
                    #api_key = ApiKey.find_by_access_token(params[:access_token])
                    #head :unauthorizied unless api_key
                    
                    authenticate_or_request_with_http_token do |token, options|
                        ApiKey.exists?(access_token: token)
                    end
                end
        end
    end
end