# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
products_list = [
  [ "Coca-Cola","CocaCola Company 1986", 350 ],
  [ "Coca-Cola Zero","CocaCola Company 1986", 340 ],
  [ "Fanta","CocaCola Company 1986", 380 ],
  [ "Sprite","CocaCola Company 1986", 335 ]
  
  
]

products_list.each do |product |
  Product.create( :name => product[0], :description => product[1], :price => product[2])
end