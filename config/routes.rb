Rails.application.routes.draw do
  get 'home/index'

  resources :products
  root to: 'home#index'
  
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :products
    end
  end
end
